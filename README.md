# Dizzy
**Dizzy** is a scss/css framework inspired by [Bootstrap](https://getbootstrap.com), made by a small group of ***devolopers*** on gitlab. Dizzy is built on SCSS/JavaScript.
## Credits
The people that helped make **dizzy** are:

1. LuigeArts (**"Lead Web Devoloper, Web Designer - Making the DF Framework Website"**)
2. DashStudios (**SCSS Variables And Half of JQuery**)
3. GeoneveStudios (**Jquery coder, 1/4 SCSS, and DF framework hosted on GeoneveStudios account**)
4. LuvidWebDesign (**SCSS Coder II & J**)
5. 0v3rs33r0v3rn34r0v3rc0d1ng(033303340301Kad) (**SCSS Coder III**)
6. FluidK0ders (**SCSS Coder IV**)
7. N00BStars (**SCSS Coder V**)
8. InspiritedBegineers (**SCSS Coder VI**)
9. M0MPlayerDesign (**SCSS Coder VII**)
10. Fr4m3w0rkD3s1gn39974 (**CSS Complier, and SCSS Coder VIII**)

###### LuigeArts™ 2021 All Rights Reserved
